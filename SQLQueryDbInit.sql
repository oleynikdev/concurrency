USE [Modules]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](256) NOT NULL,
	[Password] [nvarchar](256) NOT NULL,
	[Logname] [nvarchar](512) NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[XModels]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[XModels](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](512) NULL,
	[Health] [int] NOT NULL,
	[Modified] [datetime] NULL,
	[ModifiedBy] [nvarchar](512) NULL,
	[Version] [int] NOT NULL DEFAULT ((0)),
	[CreatedBy] [nvarchar](512) NULL,
	[Created] [datetime] NULL,
 CONSTRAINT [PK_XModels] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[YModels]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[YModels](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](512) NULL,
	[Health] [int] NOT NULL,
	[Modified] [datetime] NULL,
	[ModifiedBy] [nvarchar](512) NULL,
	[CreatedBy] [nvarchar](512) NULL,
	[Created] [datetime] NULL,
	[UserId] [int] NULL,
 CONSTRAINT [PK_YModels] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Users] ON 

GO
INSERT [dbo].[Users] ([Id], [Email], [Password], [Logname]) VALUES (1013, N'user1@fake.com', N'mELirpUhRYksFj7k8/XBcQ==', N'User')
GO
INSERT [dbo].[Users] ([Id], [Email], [Password], [Logname]) VALUES (1014, N'user2@fake.com', N'mELirpUhRYksFj7k8/XBcQ==', N'User 2')
GO
INSERT [dbo].[Users] ([Id], [Email], [Password], [Logname]) VALUES (1015, N'user3@fake.com', N'mELirpUhRYksFj7k8/XBcQ==', N'User 3')
GO
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
SET IDENTITY_INSERT [dbo].[XModels] ON 

GO
INSERT [dbo].[XModels] ([Id], [Title], [Health], [Modified], [ModifiedBy], [Version], [CreatedBy], [Created]) VALUES (39, N'For testing 1', 1, NULL, NULL, 0, N'User 5', CAST(N'2016-09-09 10:29:53.517' AS DateTime))
GO
INSERT [dbo].[XModels] ([Id], [Title], [Health], [Modified], [ModifiedBy], [Version], [CreatedBy], [Created]) VALUES (40, N'For testing 2', 2, NULL, NULL, 0, N'User 5', CAST(N'2016-09-09 10:31:23.423' AS DateTime))
GO
INSERT [dbo].[XModels] ([Id], [Title], [Health], [Modified], [ModifiedBy], [Version], [CreatedBy], [Created]) VALUES (41, N'For testing 3', 50, CAST(N'2016-09-09 10:43:07.867' AS DateTime), N'User 5', 4, N'User 5', CAST(N'2016-09-09 10:32:12.693' AS DateTime))
GO
INSERT [dbo].[XModels] ([Id], [Title], [Health], [Modified], [ModifiedBy], [Version], [CreatedBy], [Created]) VALUES (42, N'For testing 4', 4, NULL, NULL, 0, N'User 5', CAST(N'2016-09-09 10:32:49.287' AS DateTime))
GO
INSERT [dbo].[XModels] ([Id], [Title], [Health], [Modified], [ModifiedBy], [Version], [CreatedBy], [Created]) VALUES (43, N'For testing 5', 5, NULL, NULL, 0, N'User 5', CAST(N'2016-09-09 10:35:03.957' AS DateTime))
GO
INSERT [dbo].[XModels] ([Id], [Title], [Health], [Modified], [ModifiedBy], [Version], [CreatedBy], [Created]) VALUES (44, N'For testing 6', 6, NULL, NULL, 0, N'User 5', CAST(N'2016-09-09 10:35:14.297' AS DateTime))
GO
INSERT [dbo].[XModels] ([Id], [Title], [Health], [Modified], [ModifiedBy], [Version], [CreatedBy], [Created]) VALUES (45, N'For testing 7', 7, NULL, NULL, 0, N'User 5', CAST(N'2016-09-09 10:35:25.110' AS DateTime))
GO
INSERT [dbo].[XModels] ([Id], [Title], [Health], [Modified], [ModifiedBy], [Version], [CreatedBy], [Created]) VALUES (46, N'For testing 8', 8, NULL, NULL, 0, N'User 5', CAST(N'2016-09-09 10:35:33.790' AS DateTime))
GO
INSERT [dbo].[XModels] ([Id], [Title], [Health], [Modified], [ModifiedBy], [Version], [CreatedBy], [Created]) VALUES (47, N'For testing 9', 9, NULL, NULL, 0, N'User 5', CAST(N'2016-09-09 10:35:46.250' AS DateTime))
GO
INSERT [dbo].[XModels] ([Id], [Title], [Health], [Modified], [ModifiedBy], [Version], [CreatedBy], [Created]) VALUES (48, N'For testing 10', 10, NULL, NULL, 0, N'User 5', CAST(N'2016-09-09 10:35:54.407' AS DateTime))
GO
INSERT [dbo].[XModels] ([Id], [Title], [Health], [Modified], [ModifiedBy], [Version], [CreatedBy], [Created]) VALUES (49, N'For testing 11', 11, NULL, NULL, 0, N'User 5', CAST(N'2016-09-09 10:36:01.270' AS DateTime))
GO
INSERT [dbo].[XModels] ([Id], [Title], [Health], [Modified], [ModifiedBy], [Version], [CreatedBy], [Created]) VALUES (50, N'X1', 900, NULL, NULL, 0, N'User', CAST(N'2016-09-09 11:38:41.460' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[XModels] OFF
GO
SET IDENTITY_INSERT [dbo].[YModels] ON 

GO
INSERT [dbo].[YModels] ([Id], [Title], [Health], [Modified], [ModifiedBy], [CreatedBy], [Created], [UserId]) VALUES (1030, N'Y1', 1, NULL, NULL, N'User 5', CAST(N'2016-09-09 10:36:30.637' AS DateTime), NULL)
GO
INSERT [dbo].[YModels] ([Id], [Title], [Health], [Modified], [ModifiedBy], [CreatedBy], [Created], [UserId]) VALUES (1031, N'Y2', 1, NULL, NULL, N'User 5', CAST(N'2016-09-09 10:36:39.733' AS DateTime), NULL)
GO
INSERT [dbo].[YModels] ([Id], [Title], [Health], [Modified], [ModifiedBy], [CreatedBy], [Created], [UserId]) VALUES (1032, N'Y3', 3, NULL, NULL, N'User 5', CAST(N'2016-09-09 10:36:46.947' AS DateTime), NULL)
GO
INSERT [dbo].[YModels] ([Id], [Title], [Health], [Modified], [ModifiedBy], [CreatedBy], [Created], [UserId]) VALUES (1033, N'Y4', 4, NULL, NULL, N'User 5', CAST(N'2016-09-09 10:36:53.670' AS DateTime), NULL)
GO
INSERT [dbo].[YModels] ([Id], [Title], [Health], [Modified], [ModifiedBy], [CreatedBy], [Created], [UserId]) VALUES (1034, N'Y5', 5, NULL, NULL, N'User 5', CAST(N'2016-09-09 10:37:01.007' AS DateTime), NULL)
GO
INSERT [dbo].[YModels] ([Id], [Title], [Health], [Modified], [ModifiedBy], [CreatedBy], [Created], [UserId]) VALUES (1035, N'Y6', 6, NULL, NULL, N'User 5', CAST(N'2016-09-09 10:37:09.513' AS DateTime), NULL)
GO
INSERT [dbo].[YModels] ([Id], [Title], [Health], [Modified], [ModifiedBy], [CreatedBy], [Created], [UserId]) VALUES (1036, N'Y7', 7, NULL, NULL, N'User 5', CAST(N'2016-09-09 10:37:17.900' AS DateTime), NULL)
GO
INSERT [dbo].[YModels] ([Id], [Title], [Health], [Modified], [ModifiedBy], [CreatedBy], [Created], [UserId]) VALUES (1037, N'Y2', 45, NULL, NULL, N'User', CAST(N'2016-09-09 11:38:55.967' AS DateTime), NULL)
GO
SET IDENTITY_INSERT [dbo].[YModels] OFF
GO
ALTER TABLE [dbo].[YModels]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[YModels]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
/****** Object:  StoredProcedure [dbo].[BlockYModel]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Puts a lock on the row
-- =============================================
CREATE PROCEDURE [dbo].[BlockYModel]
     @id int,
	 @modified datetime,
	 @modifiedby nvarchar(512),
	 @userId int
AS
   
    UPDATE [dbo].[YModels] 
	SET UserId = @userId,
		Modified = @modified,
		ModifiedBy = @modifiedby
    WHERE Id = @id
	  AND UserId IS NULL 
GO
/****** Object:  StoredProcedure [dbo].[CheckBlockYModel]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	if there is a row that returns blocked by
-- =============================================
CREATE PROCEDURE [dbo].[CheckBlockYModel]

 @id int
AS
    SET NOCOUNT ON;
  
    SELECT ModifiedBy 
	FROM [dbo].[YModels] 
	WHERE Id = @id 
	  AND UserId IS NOT NULL

	SET NOCOUNT OFF;

GO
/****** Object:  StoredProcedure [dbo].[CheckYModel]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Сheck for the existence of row
-- =============================================
CREATE PROCEDURE [dbo].[CheckYModel]

 @id int

AS
    SET NOCOUNT ON;

    SELECT 1 FROM [dbo].[YModels] 
	WHERE Id = @id

	SET NOCOUNT OFF;

GO
/****** Object:  StoredProcedure [dbo].[DelXModel]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Deletes a row
-- =============================================
CREATE PROCEDURE [dbo].[DelXModel]
 @id int,
 @version int
AS
   
    DELETE FROM [dbo].[XModels]
	WHERE Id = @id 
	  AND [Version] = @version

GO
/****** Object:  StoredProcedure [dbo].[DelYModel]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Deletes a row
-- =============================================
CREATE PROCEDURE [dbo].[DelYModel]
 @id int
AS
    
    DELETE FROM [dbo].[YModels]
	WHERE Id = @id 
	  AND UserId IS NULL


GO
/****** Object:  StoredProcedure [dbo].[ExistsUserEmail]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Check is exists email
-- =============================================
CREATE  PROCEDURE [dbo].[ExistsUserEmail]
    @email nvarchar(256)
AS
     SET NOCOUNT ON;

     SELECT 1 FROM [dbo].[Users]
	 WHERE Email = @email

	 SET NOCOUNT OFF;

GO
/****** Object:  StoredProcedure [dbo].[GetUserByEmail]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Selects a user by a email
-- =============================================
CREATE PROCEDURE [dbo].[GetUserByEmail]
    @email nvarchar(256)
AS
     SET NOCOUNT ON;

     SELECT Id, 
	        Email, 
			Logname 
	 FROM [dbo].[Users]
	 WHERE Email = @email

	 SET NOCOUNT OFF;

GO
/****** Object:  StoredProcedure [dbo].[GetUserEmail]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Selects a email by a email and password
-- =============================================
CREATE PROCEDURE [dbo].[GetUserEmail]
    @email nvarchar(256),
	@password nvarchar(256)
AS
     SET NOCOUNT ON;

     SELECT Email FROM [dbo].[Users]
	 WHERE Email = @email
	  AND  [Password] = @password

	  SET NOCOUNT OFF;

GO
/****** Object:  StoredProcedure [dbo].[GetUsers]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Selects all users
-- =============================================
CREATE PROCEDURE [dbo].[GetUsers]
AS

    SET NOCOUNT ON;

    SELECT Id, 
        Email, 
		Logname 
    FROM [dbo].[Users] 

	SET NOCOUNT OFF;

GO
/****** Object:  StoredProcedure [dbo].[GetXModel]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Get x model by id
-- =============================================
CREATE PROCEDURE [dbo].[GetXModel]

 @id int

AS

    SET NOCOUNT ON;

    SELECT Id,
           Title,
		   Health,
		   [Version] 
	FROM [dbo].[XModels] 
	WHERE Id = @id

	SET NOCOUNT OFF;
GO
/****** Object:  StoredProcedure [dbo].[GetXModels]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Get all x models
-- =============================================
CREATE PROCEDURE [dbo].[GetXModels]
AS
    SET NOCOUNT ON;

    SELECT Id,
           Title,
		   Health,
		   [Version] 
    FROM [dbo].[XModels] 

	SET NOCOUNT OFF;

GO
/****** Object:  StoredProcedure [dbo].[GetXModelsByPageNum]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Get all x models by page number
-- =============================================
CREATE PROCEDURE [dbo].[GetXModelsByPageNum]

  @pageNumber int,
  @pageSize   int

AS
    SET NOCOUNT ON;

    SELECT Id,
           Title,
		   Health,
		   [Version] 
    FROM [dbo].[XModels]
	ORDER BY Id 
	OFFSET @pageSize * (@pageNumber - 1) ROWS
    FETCH NEXT @pageSize ROWS ONLY

	SET NOCOUNT OFF;

GO
/****** Object:  StoredProcedure [dbo].[GetYModel]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Get y model by id
-- =============================================
CREATE PROCEDURE [dbo].[GetYModel]

 @id int

AS
    SET NOCOUNT ON;

    SELECT Id,
	       Title,
		   Health,
		   UserId,
		   ModifiedBy
    FROM [dbo].[YModels] 
	WHERE Id = @id

	SET NOCOUNT OFF;


GO
/****** Object:  StoredProcedure [dbo].[GetYModels]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Get all y models
-- =============================================
CREATE PROCEDURE [dbo].[GetYModels]
AS
    SET NOCOUNT ON;

    SELECT Id,
	       Title,
		   Health,
		   UserId,
		   ModifiedBy
   FROM [dbo].[YModels] 

   SET NOCOUNT OFF;

GO
/****** Object:  StoredProcedure [dbo].[GetYModelsByPageNum]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Get all Y models by page number
-- =============================================
CREATE PROCEDURE [dbo].[GetYModelsByPageNum]

  @pageNumber int,
  @pageSize   int

AS
    SET NOCOUNT ON;

    SELECT Id,
	       Title,
		   Health,
		   UserId,
		   ModifiedBy 
    FROM [dbo].[YModels]
	ORDER BY Id 
	OFFSET @pageSize * (@pageNumber - 1) ROWS
    FETCH NEXT @pageSize ROWS ONLY

	SET NOCOUNT OFF;

GO
/****** Object:  StoredProcedure [dbo].[InsertUser]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Create new user
-- =============================================
CREATE PROCEDURE [dbo].[InsertUser]
    @email nvarchar(256),
    @logname nvarchar(256),
	@password nvarchar(512)
AS
    
    INSERT INTO [dbo].[Users] (Email, Logname, Password)
    VALUES(@email, @logname, @password)
  
    SELECT SCOPE_IDENTITY()

GO
/****** Object:  StoredProcedure [dbo].[InsertXModel]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Create new x model
-- =============================================
CREATE PROCEDURE [dbo].[InsertXModel]
    @title nvarchar(512),
    @health int,
	@created datetime,
	@createdby nvarchar(512)
AS
   
    INSERT INTO  [dbo].[XModels] (Title, Health, Created, CreatedBy)
    VALUES(@title, @health, @created, @createdby)
  
    SELECT SCOPE_IDENTITY()

GO
/****** Object:  StoredProcedure [dbo].[InsertYModel]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Create new y model
-- =============================================
CREATE PROCEDURE [dbo].[InsertYModel]
    @title nvarchar(512),
    @health int,
	@created datetime,
	@createdby nvarchar(512)
AS
    
    INSERT INTO [dbo].[YModels] (Title, Health, Created, CreatedBy)
    VALUES(@title, @health, @created, @createdby)
  
    SELECT SCOPE_IDENTITY()
GO
/****** Object:  StoredProcedure [dbo].[SchedulerUnBlockYModel]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Unblock rows by date
-- =============================================
CREATE PROCEDURE [dbo].[SchedulerUnBlockYModel]
     @date datetime
AS
    SET NOCOUNT ON;

    UPDATE [dbo].[YModels] 
	SET 
		Modified = null,
		ModifiedBy = null,
		UserId = null

    WHERE UserId IS NOT NULL
	  AND Modified <= @date

	  SET NOCOUNT OFF;

GO
/****** Object:  StoredProcedure [dbo].[UnBlockYModel]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Unblock row by id and user id
-- =============================================
CREATE PROCEDURE [dbo].[UnBlockYModel]
     @id int,	
	 @userId int
AS
   
    UPDATE [dbo].[YModels] 
	SET 
	UserId = null,
	Modified = null,
	ModifiedBy = null
    WHERE Id = @id
	  AND UserId =  @userId
GO
/****** Object:  StoredProcedure [dbo].[UnBlockYModelByUserid]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Unblock rows by user id
-- =============================================
CREATE PROCEDURE [dbo].[UnBlockYModelByUserid]
     @userId int
AS
    
    UPDATE [dbo].[YModels] 
	SET 
		Modified = null,
		ModifiedBy = null,
		UserId = null

    WHERE UserId = @userId

GO
/****** Object:  StoredProcedure [dbo].[UpdatetXModel]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Update x model
-- =============================================
CREATE PROCEDURE [dbo].[UpdatetXModel]
     @id int,
	 @version int,
     @title nvarchar(512),
     @health int,
	 @modified datetime,
	 @modifiedby nvarchar(512)
AS
    
    UPDATE [dbo].[XModels] 
	SET Title = @title,
	    Health = @health,
		[Version] = @version + 1,
		Modified = @modified,
		ModifiedBy = @modifiedby
    WHERE Id = @id
	  AND [Version] = @version 

GO
/****** Object:  StoredProcedure [dbo].[UpdatetYModel]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Update y model
-- =============================================
CREATE PROCEDURE [dbo].[UpdatetYModel]
     @id int,
	 @userid int,
     @title nvarchar(512),
     @health int
AS
    
    UPDATE [dbo].[YModels] 
	SET Title = @title,
	    Health = @health,
		Modified = null,
		ModifiedBy = null,
		UserId = null

    WHERE Id = @id
	  AND UserId = @userid

GO
/****** Object:  StoredProcedure [dbo].[XModelsCount]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Return Count X models
-- =============================================
CREATE PROCEDURE [dbo].[XModelsCount]
AS

    SET NOCOUNT ON;

    SELECT COUNT(*) 
    FROM [dbo].[XModels]

	SET NOCOUNT OFF;

GO
/****** Object:  StoredProcedure [dbo].[YModelsCount]    Script Date: 9/9/2016 11:43:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Oleynik
-- Create date: 9/8/2016
-- Description:	Return count Y models
-- =============================================
CREATE PROCEDURE [dbo].[YModelsCount]
AS

    SET NOCOUNT ON;

    SELECT COUNT(*) 
    FROM [dbo].[YModels]

	SET NOCOUNT OFF;
GO
