﻿using System;
using System.Runtime.Serialization;

namespace Concurrency.Utils
{
    public class CustomExeption: Exception
    {
        public CustomExeption(){
        }

        public CustomExeption(string message): base(message) {
        }

        public CustomExeption(string message, Exception innerException)
        : base(message, innerException) {
        }

        protected CustomExeption(SerializationInfo info, StreamingContext context)
        : base(info, context) {
        }
    }
}
