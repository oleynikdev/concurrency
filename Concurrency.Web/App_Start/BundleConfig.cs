﻿using System.Web.Optimization;

namespace Concurrency.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/angularjs").Include(
                       "~/Scripts/angular.js",
                       "~/Scripts/angular-resource.js",
                       "~/Scripts/angular-route.js",
                       "~/Scripts/angular-sanitize.js",
                       "~/Scripts/angular-ui/ui-bootstrap.js",
                       "~/Scripts/angular-ui/ui-bootstrap-tpls.js",
                        "~/Scripts/ng-confirm.js"));

            bundles.Add(new ScriptBundle("~/bundles/common").Include(
                       "~/Scripts/app/app.js",
                       "~/Scripts/app/common/BaseService.js",
                       "~/Scripts/app/common/MessagePopupCtrl.js",
                       "~/Scripts/app/common/pagination/tb-pager.js"
                       ));

            bundles.Add(new ScriptBundle("~/bundles/xmodels").Include(
                       "~/Scripts/app/xmodels/routes.js",
                       "~/Scripts/app/xmodels/services/xModelsServices.js",
                        "~/Scripts/app/xmodels/services/pagXModelServices.js",
                       "~/Scripts/app/xmodels/controllers/XModelsListCtrl.js",
                       "~/Scripts/app/xmodels/controllers/XModelCtrl.js"                       
                       ));

            bundles.Add(new ScriptBundle("~/bundles/ymodels").Include(
                      "~/Scripts/app/ymodels/routes.js",
                      "~/Scripts/app/ymodels/services/yModelsServices.js",
                      "~/Scripts/app/ymodels/services/pagYModelServices.js",
                      "~/Scripts/app/ymodels/controllers/YModelsListCtrl.js",
                      "~/Scripts/app/ymodels/controllers/YModelCtrl.js"
                      ));
        }
    }
}
