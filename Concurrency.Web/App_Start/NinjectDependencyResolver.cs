﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Ninject;
using Ninject.Web.Common;
using Concurrency.DataAccess;
using Concurrency.DataAccess.Repositories;
using Concurrency.Definitions.Common;
using Concurrency.Definitions.CurrentUser;
using Concurrency.Definitions.Users;
using Concurrency.Definitions.XModels;
using Concurrency.Definitions.YModels;
using Concurrency.Web.Services;
using Concurrency.Services.Users;
using Concurrency.Services.XModels;
using Concurrency.Services.YModels;

namespace Concurrency.Web.App_Start
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;

            AddBindings();
        }
        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }
        private void AddBindings()
        {
            kernel.Bind<IConnectionFactory>().To<AppConfigConnectionFactory>().InRequestScope()
                .WithConstructorArgument("connectionName", "DefaultConnection");

            kernel.Bind<ICurrentUserService>().To<CurrentUserService>().InRequestScope();

            kernel.Bind<IAuthenticationProvider>().To<AuthenticationProviderWrapper>().InRequestScope();
           
            kernel.Bind<IXModelsRepo>().To<XModelsRepo>().InRequestScope();
            kernel.Bind<IXModelsService>().To<XModelsService>().InRequestScope();

            kernel.Bind<IYModelsRepo>().To<YModelsRepo>().InRequestScope();
            kernel.Bind<IYModelsService>().To<YModelsService>().InRequestScope();
            kernel.Bind<IBlockService>().To<BlockService>().InRequestScope();

            kernel.Bind<IUsersRepo>().To<UsersRepo>().InRequestScope();
            kernel.Bind<IUsersService>().To<UsersService>().InRequestScope();
        }
    }
}