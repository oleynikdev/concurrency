﻿using System.Web;
using System.Web.Mvc;

namespace Concurrency.Web.Utils.ActionFilters
{
    using System.Security;

    public class IsLocalAttribute : AuthorizeAttribute
    {
        public bool ThrowSecurityException { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var isLocal = httpContext.Request.IsLocal;
            if (!isLocal && ThrowSecurityException)
                throw new SecurityException();
            return isLocal;
        }
    }
}

