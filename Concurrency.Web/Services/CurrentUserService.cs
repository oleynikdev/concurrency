﻿using System.Web;
using Concurrency.Definitions.CurrentUser;
using Concurrency.Definitions.Users;
using Concurrency.Utils;

namespace Concurrency.Web.Services
{
    public class CurrentUserService : ICurrentUserService
    {

        private readonly IUsersService _usersService;

        public CurrentUserService(
            IUsersService usersService
            )
        {
            _usersService = usersService;
        }

        public CurrentUser Get()
        {
            var currentUser = _usersService.GetByEmail(HttpContext.Current.User.Identity.Name);

            if (currentUser == null)
                throw new CustomExeption("No current user");

            return new CurrentUser
            {
               Id = currentUser.Id,
               Logname = currentUser.Logname,
               Email = currentUser.Email
            };
        }
    }
}
