﻿using System.Web.Security;
using Concurrency.Definitions.Common;

namespace Concurrency.Web.Services
{
    public class AuthenticationProviderWrapper : IAuthenticationProvider
    {
        public void RedirectFromLoginPage(string username, bool createPersistentCookie)
        {
            FormsAuthentication.SetAuthCookie(username, createPersistentCookie);
        }

        public void SetAuthCookie(string username, bool remember)
        {
            FormsAuthentication.SetAuthCookie(username, remember);
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }
    }
}
