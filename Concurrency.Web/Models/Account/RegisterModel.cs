﻿using System.ComponentModel.DataAnnotations;

namespace Concurrency.Web.Models.Account
{
    public class RegisterModel
    {
        [Required]
        [MaxLength(512)]
        public string Logname { get; set; }

        [Required]
        [MaxLength(256)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [MaxLength(256)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [MaxLength(256)]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Passwords do not match")]
        public string ConfirmPassword { get; set; }
    }
}