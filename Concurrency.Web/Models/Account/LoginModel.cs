﻿using System.ComponentModel.DataAnnotations;

namespace Concurrency.Web.Models.Account
{
    public class LoginModel
    {
        [Required]
        [MaxLength(256)]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [MaxLength(256)]
        public string Password { get; set; }
    }
}