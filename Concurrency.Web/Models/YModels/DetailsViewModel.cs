﻿using Concurrency.Definitions.YModels;

namespace Concurrency.Web.Models.YModels
{
    public class DetailsViewModel
    {
        public YModelDto Item { get; set; }        
        public int CurrentUserId { get; set; }
        public  bool Success { get; set; }
    }
}