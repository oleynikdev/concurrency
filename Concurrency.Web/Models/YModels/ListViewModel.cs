﻿using Concurrency.Definitions.YModels;

namespace Concurrency.Web.Models.YModels
{
    public class ListViewModel
    {
        public YModelDto [] Items { get; set; }

        public int TotalCount { get; set; }
        public double totalPages { get; set; }
    }
}