﻿using Concurrency.Definitions.XModels;

namespace Concurrency.Web.Models.XModels
{
    public class DetailsViewModel
    {
        public XModelDto Item { get; set; }       
         
        public int CurrentUserId { get; set; }
        public  bool Success { get; set; }
    }
}