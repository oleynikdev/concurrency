﻿using Concurrency.Definitions.XModels;

namespace Concurrency.Web.Models.XModels
{
    public class ListViewModel
    {
        public XModelDto [] Items { get; set; }

        public int TotalCount { get; set; }
        public double totalPages { get; set; }        
    }
}