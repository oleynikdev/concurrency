﻿
(function () {
    'use strict';

    angular.module('concurrencyApp',
   [
      "ngRoute",
      'ui.bootstrap',
      'ngConfirm',
      'ngResource'
   ]);
})();
