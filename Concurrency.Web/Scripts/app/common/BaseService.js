﻿(function () {
    'use strict';

    angular.module('concurrencyApp')
        .factory('baseService',
        ["$rootScope", "$http", "$q", "$window", "$interval", "$timeout",
            function ($rootScope, $http, $q, $window, $interval, $timeout) {
                return {
                    getItems: function (url) {

                        return $http.get(url);
                    },
                    getItem: function (url, id) {

                        return $http
                            .get(url,
                            {
                                params: { id: id }
                            });
                    },
                    save: function(url, data) {

                        return $http.post(url, data);
                    },
                    del: function (url, data) {
                       
                        return $http.post(url, data);
                    },
                    unblock: function (url, data) {

                        return $http.post(url, data);
                    },
                    checkblock: function (url, id) {

                        return $http.post(url,
                        {
                            id: id
                        });
                    }
                };
            }
        ]);

})();