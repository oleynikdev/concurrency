﻿
(function () {
    'use strict';

    angular.module('concurrencyApp')
      .controller('YModelsListCtrl', ["$rootScope", "$scope", "$location", '$window', '$timeout', 'yModelsServices', 'pagYModelServices',
          function ($rootScope, $scope, $location, $window, $timeout, yModelsServices, pagYModelServices) {

              $scope.currentUserId = window.currentUserId;

              $scope.create = function () {

                  $location.path('/create');
              };

              $scope.edit = function (id) {

                  yModelsServices.checkblock("/ymodels/checkblock", id).success(function (res) {

                      if (res) {

                          if (res.Success) {

                              $location.path('/edit/' + id);
                          } else {

                              $scope.message = res.Message;

                              $timeout(function () {

                                  $scope.message = null;
                                  $scope.clear();
                              },3000);
                          }
                      }
                  });
              };

              $scope.del = function (id) {

                  var data = {
                      id: id
                  };

                  yModelsServices.del("/ymodels/delete", data).success(function (res) {

                      if (res) {

                          if (res.Success) {

                              $scope.clear();
                              activate();
                          } else {

                              $scope.message = res.message;
                          }
                      }
                  });
              };

              $scope.unblock = function (id) {

                  var data = {
                      id: id
                  };

                  yModelsServices.unblock("/ymodels/unblock", data).success(function (res) {

                      $scope.clear();
                  });
              };

              //pagination section

              $scope.title = 'full paging';
              $scope.description = 'A fully paged list of items. The pager directive manages the page navigation. The fullClubService only loads a page when it clicked on for the first time';

              $scope.pages = pagYModelServices.pages;
              $scope.info = pagYModelServices.paging.info;
              $scope.options = pagYModelServices.paging.options;

              $scope.navigate = navigate;
              $scope.clear = optionsChanged;

              $scope.status = {
                  type: "info",
                  message: "ready",
                  busy: false
              };

              $scope.clear();

              function activate() {
                  //if this is the first activation of the controller load the first page
                  if (pagYModelServices.paging.info.currentPage === 0) {

                      navigate(1);
                  }
              }

              function navigate(pageNumber) {

                  $scope.status.busy = true;
                  $scope.status.message = "loading records";

                  pagYModelServices.navigate(pageNumber)
                                  .then(function () {

                                      $scope.status.message = "ready";
                                  }, function (result) {

                                      $scope.status.message = "something went wrong: " + (result.error || result.statusText);
                                  })
                                  ['finally'](function () {

                                      $scope.status.busy = false;
                                  });
              }

              function optionsChanged() {

                  pagYModelServices.clear();
                  activate();
              }
          }]);
})();


