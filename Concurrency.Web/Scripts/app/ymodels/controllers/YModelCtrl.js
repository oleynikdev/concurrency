﻿
(function () {
    'use strict';

    angular.module('concurrencyApp')
      .controller('YModelCtrl', ["$rootScope", "$scope", '$location', '$routeParams', 'yModelsServices',
          function ($rootScope, $scope, $location, $routeParams, yModelsServices) {

              var behavior = function (res) {

                  if (res) {

                      if (res.Success) {

                          $location.path('/list');
                      }

                      $scope.data = res;
                  }
              };

              yModelsServices.getItem("/ymodels/details", $routeParams.id).success(function (res) {

                  $scope.data = res;
              });

              $scope.save = function(item) {

                  yModelsServices.save("/ymodels/save", item)
                      .success(function(res) {

                          behavior(res);
                      });
              };

              $scope.unblock = function (id) {

                  var data = {
                      id: id
                  };

                  yModelsServices.unblock("/ymodels/unblock", data).success(function (res) {

                      behavior(res);
                  });
              };
          }]);
})();


