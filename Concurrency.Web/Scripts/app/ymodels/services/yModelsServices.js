﻿(function() {
    'use strict';
    
    angular.module('concurrencyApp')
        .factory('yModelsServices',
        ["$rootScope", "$http", "$q", "$window", "$interval", "$timeout", 'baseService',
            function ($rootScope, $http,  $q, $window, $interval, $timeout, baseService) {

                return {
                    getItems: function (url) {

                        return baseService.getItems(url);
                    },
                    getItem: function (url, id) {

                        return baseService.getItem(url, id);
                    },
                    save: function (url, data) {

                        return baseService.save(url, data);
                    },
                    del: function (url, data) {

                        return baseService.del(url, data);
                    },
                    unblock: function (url, data) {

                        return baseService.del(url, data);
                    },
                    checkblock: function (url, id) {

                        return baseService.checkblock(url, id);
                    }
                };
            }
        ]).factory('queryYModelServices', function ($resource) {
            return $resource("ymodels/details/:id",
                { id: "@id" },
                {                  
                    'query': {
                        method: 'GET',
                        url: '/ymodels/list/',
                        params: { pageSize: '@pageSize', pageNumber: '@pageNumber', orderBy: '@orderBy' }
                    } 
            });
        });
})();
         