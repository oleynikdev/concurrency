﻿(function() {

    angular
        .module('concurrencyApp')
        .config([
            '$routeProvider', '$logProvider',
            function($routeProvider, $logProvider) {
               
                $routeProvider
                    .when('/list', {
                        controller: 'YModelsListCtrl',
                        templateUrl: '/Templates/YModels/list.html'
                    })
                    .when('/create', {
                        controller: 'YModelCtrl',
                        templateUrl: '/Templates/YModels/edit.html'
                    })
                    .when('/edit/:id', {
                         controller: 'YModelCtrl',
                         templateUrl: '/Templates/YModels/edit.html'
                     })
                    .otherwise({
                        redirectTo: '/list'
                    });

                $logProvider.debugEnabled(true);
            }
        ]);

})();