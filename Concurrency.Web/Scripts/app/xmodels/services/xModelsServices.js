﻿(function() {
    'use strict';
    
    angular.module('concurrencyApp')
        .factory('xModelsServices',
        ["$rootScope", "$http", "$q", "$window", "$interval", "$timeout", 'baseService',
            function ($rootScope, $http,  $q, $window, $interval, $timeout, baseService) {

                return {
                    getItems: function (url) {

                        return baseService.getItems(url);
                    },
                    getItem: function (url, id) {

                        return baseService.getItem(url, id);
                    },
                    save: function (url, data) {

                        return baseService.save(url, data);
                    },
                    del: function (url, data) {

                        return baseService.del(url, data);
                    }
                };
            }
        ]).factory('queryXModelServices', function ($resource) {
            return $resource("xmodels/details/:id",
                { id: "@id" },
                {
                    'query': {
                        method: 'GET',
                        url: '/xmodels/list/',
                        params: { pageSize: '@pageSize', pageNumber: '@pageNumber', orderBy: '@orderBy' }
                    }
                });
        });

})();
         