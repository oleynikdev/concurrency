﻿
(function () {
    'use strict';

    angular.module('concurrencyApp')
      .controller('XModelCtrl', ["$rootScope", "$scope", '$location', '$routeParams', 'xModelsServices',
          function ($rootScope, $scope, $location, $routeParams, xModelsServices) {

              $scope.seltitle = null;
              $scope.selhealth = null;

              xModelsServices.getItem("/xmodels/details", $routeParams.id)
                  .success(function (item) {

                      $scope.Item = item;
                  });

              $scope.save = function () {

                  if ($scope.DbItem) {

                      $scope.Item.Version = $scope.DbItem.Version;
                  }
                  
                  xModelsServices.save("/xmodels/save", $scope.Item)
                      .success(function (res) {

                          if (res && res.Success) {

                              if (res.Item && res.DbItem) {

                                  $scope.Item = res.Item;
                                  $scope.Copy = angular.copy(res.Item);

                                  $scope.DbItem = res.DbItem;
                              } else {

                                  $location.path('/list');
                              }
                          } else {

                              $scope.Message = res.Message;
                          }
                      });
              };
          }]);
})();


