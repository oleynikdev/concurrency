﻿
(function () {
    'use strict';

    angular.module('concurrencyApp')
      .controller('XModelsListCtrl', ["$rootScope", "$scope", "$location", 'xModelsServices', 'pagXModelServices',
          function ($rootScope, $scope, $location, xModelsServices, pagXModelServices) {

              $scope.create = function () {

                  $location.path('/create');
              };

              $scope.edit = function (id) {

                  $location.path('/edit/' + id);
              };

              $scope.del = function (id, version) {

                  var data = {
                      id: id,
                      version: version
                  }

                  xModelsServices.del("/xmodels/delete", data).success(function (res) {

                      $scope.clear();
                  });
              };

              //pagination section

              $scope.title = 'full paging';
              $scope.description = 'A fully paged list of items. The pager directive manages the page navigation. The fullClubService only loads a page when it clicked on for the first time';

              $scope.pages = pagXModelServices.pages;
              $scope.info = pagXModelServices.paging.info;
              $scope.options = pagXModelServices.paging.options;

              $scope.navigate = navigate;
              $scope.clear = optionsChanged;

              $scope.status = {
                  type: "info",
                  message: "ready",
                  busy: false
              };

              $scope.clear();

              function activate() {

                  //if this is the first activation of the controller load the first page
                  if (pagXModelServices.paging.info.currentPage === 0) {

                      navigate(1);
                  }
              }

              function navigate(pageNumber) {

                  $scope.status.busy = true;
                  $scope.status.message = "loading records";

                  pagXModelServices.navigate(pageNumber)
                                  .then(function () {

                                      $scope.status.message = "ready";
                                  }, function (result) {

                                      $scope.status.message = "something went wrong: " + (result.error || result.statusText);
                                  })
                                  ['finally'](function () {

                                      $scope.status.busy = false;
                                  });
              }

              function optionsChanged() {

                  pagXModelServices.clear();
                  activate();
              }
          }]);
})();


