﻿(function() {

    angular
        .module('concurrencyApp')
        .config([
            '$routeProvider', '$logProvider',
            function($routeProvider, $logProvider) {
               
                $routeProvider
                    .when('/list', {
                        controller: 'XModelsListCtrl',
                        templateUrl: '/Templates/XModels/list.html',
                        reloadOnSearch: false
                    })
                    .when('/create', {
                        controller: 'XModelCtrl',
                        templateUrl: '/Templates/XModels/edit.html'
                    })
                    .when('/edit/:id', {
                         controller: 'XModelCtrl',
                         templateUrl: '/Templates/XModels/edit.html'
                     })
                    .otherwise({
                        redirectTo: '/list'
                    });

                $logProvider.debugEnabled(true);
            }
        ]);

})();