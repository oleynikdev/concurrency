﻿using System;
using System.Web.Mvc;
using Concurrency.Definitions.YModels;
using Concurrency.Web.Utils.ActionFilters;

namespace Concurrency.Web.Controllers
{
    [IsLocal]
    public class SchedulerController : Controller
    {
        private readonly IBlockService _blockService;

        public SchedulerController(
            IBlockService blockService
            )
        {
            _blockService = blockService;
        }

        [Route("scheduler/unblock")]
        [OutputCache(NoStore = true, Duration = 0)]
        [HttpGet]
        public void Unblock()
        {
            _blockService.SchedulerUnBlock(DateTime.Now.AddHours(-4));
        }
    }
}