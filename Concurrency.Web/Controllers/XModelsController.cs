﻿using System;
using System.Linq;
using System.Web.Mvc;
using Concurrency.Definitions.CurrentUser;
using Concurrency.Definitions.XModels;
using Concurrency.Web.Models.XModels;
using Concurrency.Utils;

namespace Concurrency.Web.Controllers
{
    [Authorize]
    public class XModelsController : Controller
    {
        #region Constant

        const string MessageSystemEx = "System exeption";

        #endregion

        private readonly IXModelsService _xmodelsService;
        private readonly ICurrentUserService _currentUser;
        private readonly log4net.ILog _logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public XModelsController(
            IXModelsService xmodelsService,
            ICurrentUserService currentUser
            )
        {
            _xmodelsService = xmodelsService;
            _currentUser = currentUser;
        }

        public ActionResult Index()
        {
            ViewBag.CurrentUser = _currentUser.Get();

            return View();
        }

        [Route("xmodels/list")]
        [OutputCache(NoStore = true, Duration = 0)]
        public ActionResult List(int pageSize, int pageNumber, string orderBy = null)
        {   
            var totalCount = _xmodelsService.GetCount();
            var totalPages = Math.Ceiling((double)totalCount / pageSize);
            
            return Json(new ListViewModel
            {
                TotalCount = totalCount,
                totalPages = totalPages,
                Items = _xmodelsService.GetItemList(pageNumber, pageSize).ToArray()
            }, JsonRequestBehavior.AllowGet);
        }

        [Route("xmodels/details")]
        [OutputCache(NoStore = true, Duration = 0)]
        public ActionResult Details(int? id)
        {
            var model = id.HasValue ? _xmodelsService.GetItem(id.Value)
                                    : new XModelDto();

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Route("xmodels/save")]
        public ActionResult Save(XModelDto model)
        {
            if (model == null)
                return HttpNotFound();

            try
            {  
                return Json(_xmodelsService.Save(model));
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);

                return Json(new
                {
                    Success = false,
                    Message = ex is CustomExeption ? ex.Message : MessageSystemEx
                });
            }
        }

        [HttpPost]
        [Route("xmodels/delete")]
        public ActionResult Delete(int id, int version)
        {
            try
            {
                _xmodelsService.Delete(id, version);

                return Json(new
                {
                    success = true
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);

                return Json(new
                {
                    success = false,
                    Message = ex is CustomExeption ? ex.Message : MessageSystemEx
                });
            }
        }
    }
}