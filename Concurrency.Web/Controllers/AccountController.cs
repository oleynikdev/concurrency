﻿using System.Web.Mvc;
using Concurrency.Definitions.Common;
using Concurrency.Definitions.Users;
using Concurrency.Definitions.YModels;
using Concurrency.Web.Models.Account;
using Concurrency.Web.Utils;

namespace Concurrency.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUsersService _usersService;
        private readonly IBlockService _blockService;
        private readonly IAuthenticationProvider _authenticationProvider;

        public AccountController(
            IUsersService usersService,
            IBlockService blockService,
            IAuthenticationProvider authenticationProvider
            )
        {
            _usersService = usersService;
            _blockService = blockService;
            _authenticationProvider = authenticationProvider;
        }
        
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                object emal = _usersService.GetEmail(model.Email.Trim(), EncryptHelper.Encrypt(model.Password.Trim()));

                if (emal != null)
                {
                    _authenticationProvider.SetAuthCookie(emal.ToString(), true);

                    return RedirectToAction("Index", "Home");
                }

                ModelState.AddModelError(string.Empty, "User with such password and email are no");
            }

            return View(model);
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                model.Email = model.Email.Trim();

                if (!_usersService.ExistsEmail(model.Email))
                {   
                    _usersService.Create(new UserDto
                    {
                        Logname = model.Logname,
                        Email = model.Email,
                        Password = EncryptHelper.Encrypt(model.Password.Trim())
                    });

                    _authenticationProvider.SetAuthCookie(model.Email, true);

                    return RedirectToAction("Index", "Home");
                }
                
                ModelState.AddModelError(string.Empty, "A user with this email already exists");
            }

            return View(model);
        }

        public ActionResult Logoff()
        {
            _blockService.UnBlockByCurrentUser();

            _authenticationProvider.SignOut();
           
            return RedirectToAction("Index", "Home");
        }
    }
}