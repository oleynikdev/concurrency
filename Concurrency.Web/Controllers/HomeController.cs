﻿using System.Web.Mvc;

namespace Concurrency.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return Redirect("XModels");
        }
    }
}