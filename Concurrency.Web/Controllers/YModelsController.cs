﻿using System;
using System.Linq;
using System.Web.Mvc;
using Concurrency.Definitions.CurrentUser;
using Concurrency.Definitions.YModels;
using Concurrency.Web.Models.YModels;
using Concurrency.Utils;

namespace Concurrency.Web.Controllers
{
    [Authorize]
    public class YModelsController : Controller
    {
        #region Constant

        const string MessageSystemEx = "System exeption";

        #endregion

        private readonly IYModelsService _yModelsService;
        private readonly ICurrentUserService _currentUser;
        private readonly IBlockService _blockService;

        private readonly log4net.ILog _logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public YModelsController(
            IYModelsService yModelsService,
            ICurrentUserService currentUser,
            IBlockService blockService
            )
        {
            _yModelsService = yModelsService;
            _currentUser = currentUser;
            _blockService = blockService;
        }

        public ActionResult Index()
        {
            ViewBag.CurrentUser = _currentUser.Get();

            return View();
        }

        [Route("ymodels/list")]
        [OutputCache(NoStore = true, Duration = 0)]
        public ActionResult List(int pageSize, int pageNumber, string orderBy = "")
        {
            var totalCount = _yModelsService.GetCount();
            var totalPages = Math.Ceiling((double)totalCount / pageSize);
           
            return Json(new ListViewModel
            {
                TotalCount = totalCount,
                totalPages =  totalPages,
                Items = _yModelsService.GetItemList(pageNumber, pageSize).ToArray()
            }, JsonRequestBehavior.AllowGet);
        }

        [Route("ymodels/details")]
        [OutputCache(NoStore = true, Duration = 0)]
        public ActionResult Details(int? id)
        {
            try
            {
                var model = id.HasValue
                    ? _yModelsService.GetItem(id.Value)
                    : new YModelDto();

                var currentUser = _currentUser.Get();

                return Json(new DetailsViewModel
                {
                    Success = true,
                    Item = model,
                    CurrentUserId = currentUser.Id
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);

                return Json(new
                {
                    Success = false,
                    Message = ex is CustomExeption ? ex.Message : MessageSystemEx
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [Route("ymodels/save")]
        public ActionResult Save(YModelDto model)
        {   
            if (model == null)
                return HttpNotFound();
            
            try
            {
                _yModelsService.Save(model);

                return Json(new
                {
                    Success = true
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);

                return Json(new
                {
                    Success = false,
                    Message = ex is CustomExeption ? ex.Message : MessageSystemEx
                });
            }
        }

        [HttpPost]
        [Route("ymodels/delete")]
        public ActionResult Delete(int id)
        {
            try
            {
                _yModelsService.Delete(id);

                return Json(new
                {
                    Success = true
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);

                return Json(new
                {
                    Success = false,
                    Message = ex is CustomExeption ? ex.Message : MessageSystemEx
                });
            }
        }

        [HttpPost]
        [Route("ymodels/unblock")]
        public ActionResult Unblock(int id)
        {
            try
            {
                _blockService.UnBlock(id);

                return Json(new
                {
                    Success = true
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);

                return Json(new
                {
                    Success = false,
                    Message = ex is CustomExeption ? ex.Message : MessageSystemEx
                });
            }
        }

        [HttpPost]
        [Route("ymodels/checkblock")]
        public ActionResult CheckBlock(int id)
        {
            try
            {
                _blockService.CheckBlock(id);

                return Json(new
                {
                    Success = true
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);

                return Json(new
                {
                    Success = false,
                    Message = ex is CustomExeption ? ex.Message : MessageSystemEx
                });
            }
        }
    }
}