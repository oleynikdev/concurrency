﻿using System;
using Quartz;
using Concurrency.Definitions.YModels;

namespace Concurrency.Web.Jobs
{
    public class UnblockedJob: IJob
    {
        private readonly IBlockService _blockService;

        public UnblockedJob(
            IBlockService blockService
            )
        {
            _blockService = blockService;
        }

        public void Execute(IJobExecutionContext context)
        {
            _blockService.SchedulerUnBlock(DateTime.Now.AddHours(-1));
        }
    }
}