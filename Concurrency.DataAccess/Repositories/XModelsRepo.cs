﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Concurrency.DataAccess.Repositories.Base;
using Concurrency.Definitions.Common;
using Concurrency.Definitions.XModels;

namespace Concurrency.DataAccess.Repositories
{
    public class XModelsRepo : BaseRepository<XModelDto>, IXModelsRepo
    {
        #region Constant

        private const string SpInsertXModel = "InsertXModel";
        private const string SpGetXModels = "GetXModels";
        private const string SpGetXModel = "GetXModel";
        private const string SpDelXModel = "DelXModel";
        private const string SpUpdatetXModel = "UpdatetXModel";
        private const string SpXModelsCount = "XModelsCount";
        private const string SpGetXModelsByPageNum = "GetXModelsByPageNum";

        #endregion

        private readonly IConnectionFactory _connectionFactory;

        public XModelsRepo(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public XModelDto GetItem(int id)
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpGetXModel;

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@id",
                        Value = id
                    });

                    return ToItem(command);
                }
            }
        }

        public int GetCount()
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpXModelsCount;
                    
                    return (int)command.ExecuteScalar();
                }
            }
        }

        public void Create(XModelDto item)
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpInsertXModel;

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@title",
                        Value = item.Title
                    });

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@health",
                        Value = item.Health
                    });

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@created",
                        Value = item.Created
                    });

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@createdby",
                        Value = item.CreatedBy
                    });

                    command.ExecuteScalar();
                }
            }
        }

        public void Update(XModelDto item)
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpUpdatetXModel;

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@id",
                        Value = item.Id
                    });

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@version",
                        Value = item.Version
                    });
                    
                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@title",
                        Value = item.Title
                    });

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@health",
                        Value = item.Health
                    });

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@modified",
                        Value = item.Modified
                    });

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@modifiedby",
                        Value = item.ModifiedBy
                    });

                    command.ExecuteNonQuery();
                }
            }
        }

        public void Delete(int id, int version)
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpDelXModel;

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@id",
                        Value = id
                    });

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@version",
                        Value = version
                    });

                    command.ExecuteNonQuery();
                }
            }
        }

        public IEnumerable<XModelDto> GetItemList()
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpGetXModels;

                    return ToList(command);
                }
            }
        }

        public IEnumerable<XModelDto> GetItemList(int pageNumber, int pageSize)
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpGetXModelsByPageNum;

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pageNumber",
                        Value = pageNumber
                    });

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pageSize",
                        Value = pageSize
                    });

                    return ToList(command);
                }
            }
        }

        protected override void Map(IDataRecord record, XModelDto item)
        {
            item.Id = (int)record["Id"];
            item.Title = record["Title"] == DBNull.Value ? string.Empty : record["Title"].ToString();
            item.Health = (int)record["Health"];

            item.Version = (int)record["Version"];
        }
    }
}
