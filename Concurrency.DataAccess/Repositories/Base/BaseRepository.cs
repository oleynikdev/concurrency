﻿using System.Collections.Generic;
using System.Data;
using Concurrency.Definitions.Common;

namespace Concurrency.DataAccess.Repositories.Base
{
    public abstract class BaseRepository<TEntity> where TEntity :  class, IEntity, new()
    {
        protected IEnumerable<TEntity> ToList(IDbCommand command)
        {
            using (var reader = command.ExecuteReader())
            {
                List<TEntity> items = new List<TEntity>();

                while (reader.Read())
                {
                    var item = new TEntity();

                    Map(reader, item);
                    items.Add(item);
                }
                return items;
            }
        }

        protected TEntity ToItem(IDbCommand command)
        {
            using (var reader = command.ExecuteReader())
            {
                if (reader.Read())
                {
                    TEntity item = new TEntity();

                    Map(reader, item);

                    return item;
                }

                return null;
            }
        }

        protected object ToField(IDbCommand command, string name)
        {
            using (var reader = command.ExecuteReader())
            {
                object result = null;

                if (reader.Read())
                    result = reader[name];


                return result;
            }
        }

        protected abstract void Map(IDataRecord record, TEntity entity);
    }
}
