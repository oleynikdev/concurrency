﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Concurrency.DataAccess.Repositories.Base;
using Concurrency.Definitions.Common;
using Concurrency.Definitions.YModels;

namespace Concurrency.DataAccess.Repositories
{
    public class YModelsRepo : BaseRepository<YModelDto>, IYModelsRepo
    {
        #region Constant

        private const string SpInsertYModel = "InsertYModel";
        private const string SpGetYModels = "GetYModels";
        private const string SpGetYModel = "GetYModel";
        private const string SpDelYModel = "DelYModel";
        private const string SpUpdatetYModel = "UpdatetYModel";
        private const string SpBlockedYModel = "BlockYModel";
        private const string SpUnBlockYModel = "UnBlockYModel";
        private const string SpCheckYModel = "CheckYModel";
        private const string SpSchedulerUnBlockYModel = "SchedulerUnBlockYModel";
        private const string SpCheckBlockYModel = "CheckBlockYModel";
        private const string SpUnBlockYModelByUserid = "UnBlockYModelByUserid";
        private const string SpYModelsCount = "YModelsCount";
        private const string SpGetXModelsByPageNum = "GetYModelsByPageNum";

        #endregion

        private readonly IConnectionFactory _connectionFactory;

        public YModelsRepo(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        #region Block

        public int Block(int id, string modifiedBy, int userId)
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpBlockedYModel;

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@id",
                        Value = id
                    });

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@modified",
                        Value = DateTime.Now
                    });

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@modifiedby",
                        Value = modifiedBy
                    });

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@userId",
                        Value = userId
                    });

                    return command.ExecuteNonQuery();
                }
            }
        }
        
        public int UnBlock(int id, int userId)
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpUnBlockYModel;

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@id",
                        Value = id
                    });
                    
                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@userId",
                        Value = userId
                    });

                    return command.ExecuteNonQuery();
                }
            }
        }

        public void SchedulerUnBlock(DateTime date)
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpSchedulerUnBlockYModel;

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@date",
                        Value = date
                    });

                    command.ExecuteNonQuery();
                }
            }
        }

        public object CheckBlock(int id)
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpCheckBlockYModel;

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@id",
                        Value = id
                    });

                    return ToField(command, "ModifiedBy");
                }
            }
        }

        public void UnBlockByUserId(int userId)
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpUnBlockYModelByUserid;

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@userId",
                        Value = userId
                    });

                    command.ExecuteNonQuery();
                }
            }
        }

        #endregion

        public int GetCount()
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpYModelsCount;

                    return (int)command.ExecuteScalar();
                }
            }
        }

        public YModelDto GetItem(int id)
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {  
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpGetYModel;

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@id",
                        Value = id
                    });

                    return ToItem(command);
                }
            }
        }
        
        public bool CheckItem(int id)
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpCheckYModel;

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@id",
                        Value = id
                    });

                    return command.ExecuteScalar() == null;
                }
            }
        }
        
        public void Create(YModelDto item)
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpInsertYModel;

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@title",
                        Value = item.Title
                    });

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@health",
                        Value = item.Health
                    });

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@created",
                        Value = item.Created
                    });

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@createdby",
                        Value = item.CreatedBy
                    });

                    command.ExecuteScalar();
                }
            }
        }

        public void Update(YModelDto item, int userId)
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpUpdatetYModel;

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@id",
                        Value = item.Id
                    });

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@userid",
                        Value = userId
                    });

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@title",
                        Value = item.Title
                    });

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@health",
                        Value = item.Health
                    });
                    
                    command.ExecuteNonQuery();
                }
            }
        }

        public bool Delete(int id)
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpDelYModel;

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@id",
                        Value = id
                    });
                    
                   return command.ExecuteNonQuery() == 1;
                }
            }
        }

        public IEnumerable<YModelDto> GetItemList()
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpGetYModels;

                    return ToList(command);
                }
            }
        }

        public IEnumerable<YModelDto> GetItemList(int pageNumber, int pageSize)
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpGetXModelsByPageNum;

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pageNumber",
                        Value = pageNumber
                    });

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pageSize",
                        Value = pageSize
                    });

                    return ToList(command);
                }
            }
        }

        protected override void Map(IDataRecord record, YModelDto item)
        {
            item.Id = (int)record["Id"];
            item.Title = record["Title"] == DBNull.Value ? string.Empty : record["Title"].ToString();
            item.Health = (int)record["Health"];

            item.UserId = record["UserId"] == DBNull.Value ? null : (int?)record["UserId"];
            item.ModifiedBy = record["ModifiedBy"] == DBNull.Value ? string.Empty : record["ModifiedBy"].ToString();
        }
    }
}
