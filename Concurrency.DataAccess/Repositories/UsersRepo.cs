﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Concurrency.DataAccess.Repositories.Base;
using Concurrency.Definitions.Common;
using Concurrency.Definitions.Users;

namespace Concurrency.DataAccess.Repositories
{
    public class UsersRepo : BaseRepository<UserDto>, IUsersRepo
    {
        #region Constant

        private const string SpInsertUser = "InsertUser";
        private const string SpGetUsers = "GetUsers";
        private const string SpGetUserByEmail = "GetUserByEmail";
        private const string SpExistsUserEmail = "ExistsUserEmail";
        private const string SpGetUserEmail = "GetUserEmail";

        #endregion

        private readonly IConnectionFactory _connectionFactory;

        public UsersRepo(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public void Create(UserDto item)
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpInsertUser;
                    
                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@email",
                        Value = item.Email
                    });

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@logname",
                        Value = item.Logname
                    });

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@password",
                        Value = item.Password
                    });

                    command.ExecuteScalar();
                }
            }
           
        }
       
        public UserDto GetByEmail(string email)
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpGetUserByEmail;

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@email",
                        Value = email
                    });

                    return ToItem(command);
                }
            }
        }

        public object GetEmail(string email, string password)
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpGetUserEmail;

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@email",
                        Value = email
                    });

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@password",
                        Value = password
                    });
                    
                    return ToField(command, "Email");
                }
            }
        }

        public bool ExistsEmail(string email)
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpExistsUserEmail;

                    command.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@email",
                        Value = email
                    });
                    
                    return command.ExecuteScalar() != null;
                }
            }
        }

        public IEnumerable<UserDto> GetItemList()
        {
            using (var connection = _connectionFactory.Create())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = SpGetUsers;
                    
                    return ToList(command);
                }
            }
        }
        
        protected override void Map(IDataRecord record, UserDto item)
        {
            item.Id = (int)record["Id"];
            item.Email = (string)record["Email"];
            item.Logname = (string)record["Logname"];
        }
    }
}
