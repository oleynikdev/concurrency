﻿
namespace Concurrency.Definitions.CurrentUser
{
    public interface ICurrentUserService
    {
        CurrentUser Get();
    }
}
