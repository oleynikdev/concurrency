﻿
namespace Concurrency.Definitions.CurrentUser
{
   public class CurrentUser
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Logname { get; set; }
    }
}
