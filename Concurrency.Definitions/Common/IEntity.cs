﻿
namespace Concurrency.Definitions.Common
{
   public interface IEntity
    {
        int Id { get; set; }
    }
}
