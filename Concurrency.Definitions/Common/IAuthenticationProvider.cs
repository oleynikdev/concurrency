﻿
namespace Concurrency.Definitions.Common
{
    public interface IAuthenticationProvider
    {
        void SignOut();
        void SetAuthCookie(string username, bool remember);
        void RedirectFromLoginPage(string username, bool createPersistentCookie);
    }
}
