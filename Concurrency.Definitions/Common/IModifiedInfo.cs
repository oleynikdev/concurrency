﻿using System;

namespace Concurrency.Definitions.Common
{
   public interface IModifiedInfo
    {
        DateTime? Modified { get; set; }
        string ModifiedBy { get; set; }
    }
}
