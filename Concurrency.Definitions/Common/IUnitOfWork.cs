﻿using System;
using System.Data;

namespace Concurrency.Definitions.Common
{
    public interface IUnitOfWork: IDisposable
    {
        void SaveChanges();
        IDbTransaction Transaction { get;}
    }
}
