﻿using System;
using System.Data;

namespace Concurrency.Definitions.Common
{
    public interface IAdoNetContext: IDisposable
    {
         IUnitOfWork CreateUnitOfWork();        
         IDbCommand CreateCommand();
    }
}
