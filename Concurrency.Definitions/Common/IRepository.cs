﻿using System;
using System.Collections.Generic;

namespace Concurrency.Definitions.Common
{
    public interface IRepository<TEntity> : IDisposable
        where TEntity : class, IEntity, new()
    {
        IEnumerable<TEntity> GetItemList();
        TEntity GetItem(int id);
        void Create(TEntity item);
        void Update(TEntity item); 
        void Delete(int id); 
        void Save();
    }
}
