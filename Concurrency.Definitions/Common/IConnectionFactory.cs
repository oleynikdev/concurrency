﻿
using System.Data;

namespace Concurrency.Definitions.Common
{
   public interface IConnectionFactory
   {
       IDbConnection Create();
   }
}
