﻿using Concurrency.Definitions.Common;

namespace Concurrency.Definitions.Users
{
   public interface IUserInfo: IEntity
    {   
        string Email { get; set; }
        string Logname { get; set; }
        string Password { get; set; }
    }
}
