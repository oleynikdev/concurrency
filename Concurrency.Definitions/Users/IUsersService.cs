﻿using System.Collections.Generic;

namespace Concurrency.Definitions.Users
{
    public interface IUsersService
    {
        IEnumerable<UserDto> GetItemList();
        void Create(UserDto item);
        UserDto GetByEmail(string email);
        bool ExistsEmail(string email);
        object GetEmail(string email, string password);
    }
}
