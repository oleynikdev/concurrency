﻿
namespace Concurrency.Definitions.Users
{
   public class UserDto: IUserInfo
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Logname { get; set; }
        public string Password { get; set; }
    }
}
