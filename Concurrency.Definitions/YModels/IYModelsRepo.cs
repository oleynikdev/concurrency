﻿using System;
using System.Collections.Generic;

namespace Concurrency.Definitions.YModels
{
   public interface IYModelsRepo
    {
        IEnumerable<YModelDto> GetItemList();
        IEnumerable<YModelDto> GetItemList(int pageNumber, int pageSize);
        void Create(YModelDto item);
        int GetCount();
        YModelDto GetItem(int id);
        int Block(int id, string modifiedBy, int userId);
        int UnBlock(int id, int userId);
        bool Delete(int id);
        void Update(YModelDto item, int userId);
        bool CheckItem(int id);
        void SchedulerUnBlock(DateTime date);
        object CheckBlock(int id);
        void UnBlockByUserId(int userId);
    }
}
