﻿using System;

namespace Concurrency.Definitions.YModels
{
   public interface IBlockService
    {  
        int Block(int id);
        int UnBlock(int id);
        void SchedulerUnBlock(DateTime date);
        void CheckBlock(int id);
        void UnBlockByCurrentUser();
    }
}
