﻿
namespace Concurrency.Definitions.YModels
{
   public interface IYModifiedInfo
    {  
        int? UserId { get; set; }
    }
}
