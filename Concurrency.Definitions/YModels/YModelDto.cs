﻿using System;
using Concurrency.Definitions.Common;

namespace Concurrency.Definitions.YModels
{
   public class YModelDto: IYModelInfo, IModifiedInfo, IYModifiedInfo
    {
        public int Id { get; set; }
        public int Health { get; set; }
        public string Title { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }

        public DateTime? Modified { get; set; }
        public string ModifiedBy { get; set; }

        public int? UserId { get; set; }
    }
}
