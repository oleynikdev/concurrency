﻿using System;
using Concurrency.Definitions.Common;

namespace Concurrency.Definitions.YModels
{
   public interface IYModelInfo: IEntity
    {
        string Title { get; set; }
        int Health { get; set; }
        DateTime Created { get; set; }
        string CreatedBy { get; set; }
    }
}
