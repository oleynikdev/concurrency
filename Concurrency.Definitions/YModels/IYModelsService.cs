﻿using System.Collections.Generic;

namespace Concurrency.Definitions.YModels
{
   public interface IYModelsService
   {
        IEnumerable<YModelDto> GetItemList();
        IEnumerable<YModelDto> GetItemList(int pageNumber, int pageSize);
        YModelDto GetItem(int id);
        void Delete(int id);
        void Save(YModelDto item);
        int GetCount();
   }
}
