﻿
namespace Concurrency.Definitions.XModels
{
   public class UpdateResultXModel
    {
      public  XModelDto Item { get; set; }
      public  XModelDto DbItem { get; set; }

      public  bool Success { get; set; }
    }
}
