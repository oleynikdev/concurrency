﻿
namespace Concurrency.Definitions.XModels
{
   public interface IXModifiedInfo
    {  
        int Version { get; set; }
    }
}
