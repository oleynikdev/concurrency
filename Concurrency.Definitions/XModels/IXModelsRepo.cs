﻿using System.Collections.Generic;

namespace Concurrency.Definitions.XModels
{
    public interface IXModelsRepo
    {
        IEnumerable<XModelDto> GetItemList();
        IEnumerable<XModelDto> GetItemList(int pageNumber, int pageSize);
        void Create(XModelDto item);
        int GetCount();
        XModelDto GetItem(int id);
        void Delete(int id, int version);
        void Update(XModelDto item);
    }
}
