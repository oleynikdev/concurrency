﻿using System.Collections.Generic;

namespace Concurrency.Definitions.XModels
{
    public interface IXModelsService
    {
        IEnumerable<XModelDto> GetItemList();
        IEnumerable<XModelDto> GetItemList(int pageNumber, int pageSize);
        XModelDto GetItem(int id);
        void Delete(int id, int version);
        UpdateResultXModel Save(XModelDto item);
        int GetCount();
    }
}
