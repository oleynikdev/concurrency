﻿using System;
using Concurrency.Definitions.Common;

namespace Concurrency.Definitions.XModels
{
   public interface IXModelInfo: IEntity
    {
        string Title { get; set; }
        int Health { get; set; }
        DateTime Created { get; set; }
        string CreatedBy { get; set; }
    }
}
