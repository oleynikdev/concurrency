﻿using System;
using Concurrency.Definitions.Common;

namespace Concurrency.Definitions.XModels
{
   public class XModelDto: IXModelInfo,IModifiedInfo, IXModifiedInfo
    {
        public int Id { get; set; }
        public int Health { get; set; }
        public string Title { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }

        public DateTime? Modified { get; set; }
        public string ModifiedBy { get; set; }
        public int Version { get; set; }
    }
}
