﻿using System;
using System.Collections.Generic;
using Concurrency.Definitions.CurrentUser;
using Concurrency.Definitions.XModels;
using Concurrency.Utils;

namespace Concurrency.Services.XModels
{
    public class XModelsService : IXModelsService
    {
        private readonly IXModelsRepo _repo;
        private readonly ICurrentUserService _currentUser;

        public XModelsService(
            IXModelsRepo repo,
            ICurrentUserService currentUser
            )
        {
            _repo = repo;
            _currentUser = currentUser;
        }

        public UpdateResultXModel Save(XModelDto item)
        {
            var currentUser = _currentUser.Get();

            if (item.Id > 0)
            {
                var dbItem = _repo.GetItem(item.Id);

                if (dbItem == null)
                   throw new CustomExeption("The current row does not exist");

                if (dbItem.Version == item.Version)
                {

                    item.Modified = DateTime.Now;
                    item.ModifiedBy = currentUser.Logname;

                    _repo.Update(item);
                }
                else
                {
                    return new UpdateResultXModel
                    {
                        Success = true,
                        Item = item,
                        DbItem = dbItem
                    };
                }
            }
            else
            {
                item.CreatedBy = currentUser.Logname;
                item.Created = DateTime.Now;

                _repo.Create(item);
            }

            return new UpdateResultXModel
            {
                Success = true
            };
        }

        public int GetCount()
        {
          return _repo.GetCount();
        }

        public void Delete(int id, int version)
        {
            _repo.Delete(id, version);
        }

        public IEnumerable<XModelDto> GetItemList(int pageNumber, int pageSize)
        {
            return _repo.GetItemList(pageNumber, pageSize);
        }

        public XModelDto GetItem(int id)
        {
           return _repo.GetItem(id);
        }

        public IEnumerable<XModelDto> GetItemList()
        {
           return _repo.GetItemList();
        }        
    }
}
