﻿using System;
using Concurrency.Definitions.CurrentUser;
using Concurrency.Definitions.YModels;
using Concurrency.Utils;

namespace Concurrency.Services.YModels
{
    public class BlockService : IBlockService
    {
        private readonly IYModelsRepo _repo;
        private readonly ICurrentUserService _currentUser;

        public BlockService(
            IYModelsRepo repo,
            ICurrentUserService currentUser
            )
        {
            _repo = repo;
            _currentUser = currentUser;
        }

        public int Block(int id)
        {
            var currentUser = _currentUser.Get();

            if(currentUser == null)
                throw  new CustomExeption("No current user");

            return _repo.Block(id, currentUser.Logname, currentUser.Id);
        }

        public int UnBlock(int id)
        {
            var currentUser = _currentUser.Get();

            if (currentUser == null)
                throw new CustomExeption("No current user");

            return _repo.UnBlock(id, currentUser.Id);
        }

        public void SchedulerUnBlock(DateTime date)
        {
            _repo.SchedulerUnBlock(date);
        }

        public void CheckBlock(int id)
        {
            var blockedBy = _repo.CheckBlock(id);

            if (blockedBy != null)
                throw new CustomExeption(string.Format("The row is already blocked by {0}", blockedBy));

        }

        public void UnBlockByCurrentUser()
        {
            var currentUser = _currentUser.Get();

           _repo.UnBlockByUserId(currentUser.Id);
        }
    }
}
