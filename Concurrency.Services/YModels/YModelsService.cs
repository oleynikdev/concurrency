﻿using System;
using System.Collections.Generic;
using Concurrency.Definitions.CurrentUser;
using Concurrency.Definitions.YModels;
using Concurrency.Utils;

namespace Concurrency.Services.YModels
{
    public class YModelsService : IYModelsService
    {
        private readonly IYModelsRepo _repo;
        private readonly ICurrentUserService _currentUser;
        private readonly IBlockService _blockService;

        public YModelsService(
            IYModelsRepo repo,
            ICurrentUserService currentUser,
            IBlockService blockService
            )
        {
            _repo = repo;
            _currentUser = currentUser;
            _blockService = blockService;
        }

        public void Save(YModelDto item)
        {
            var currentUser = _currentUser.Get();

            if (item.Id > 0)
            {   
                _repo.Update(item, currentUser.Id);
            }
            else
            {
                item.CreatedBy = currentUser.Logname;
                item.Created = DateTime.Now;

                _repo.Create(item);
            }
        }

        public int GetCount()
        {
            return _repo.GetCount();
        }

        public void Delete(int id)
        {
            if(_repo.CheckItem(id))
                throw new CustomExeption("It is already removed");

            if (!_repo.Delete(id))
                throw new CustomExeption("You can not delete locked row");
        }

        public IEnumerable<YModelDto> GetItemList(int pageNumber, int pageSize)
        {
           return _repo.GetItemList(pageNumber, pageSize);
        }

        public YModelDto GetItem(int id)
        {
            var currentUser = _currentUser.Get();
            var model = _repo.GetItem(id);

            if (_blockService.Block(id) == 1 
                 || model.UserId == currentUser.Id)
                return model;
            
            throw new CustomExeption(string.Format("The row is already blocked by {0}", model.ModifiedBy));
        }

        public IEnumerable<YModelDto> GetItemList()
        {
            return _repo.GetItemList();
        }
    }
}
