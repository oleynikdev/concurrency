﻿using System.Collections.Generic;
using Concurrency.Definitions.Users;

namespace Concurrency.Services.Users
{
    public class UsersService : IUsersService
    {
        private readonly IUsersRepo _repo;

        public UsersService(IUsersRepo repo)
        {
            _repo = repo;
        }

        public void Create(UserDto item)
        {
            _repo.Create(item);
        }

        public bool ExistsEmail(string email)
        {
            return _repo.ExistsEmail(email);
        }

        public UserDto GetByEmail(string email)
        {
            return _repo.GetByEmail(email);
        }

        public object GetEmail(string email, string password)
        {
            return _repo.GetEmail(email, password);
        }

        public IEnumerable<UserDto> GetItemList()
        {
           return _repo.GetItemList();
        }
    }
}
